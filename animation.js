var cloud1 = 180; var cloud2 = 720;
var bt1 = 0; var bt2 = 216; var bt3 = 432; var bt4 = 648; var bt5 = 864;
var ft1 = 0; var ft2 = 300; var ft3 = 600; var ft4 = 900; var fencex = 0;
var airplane = 1000
var air = 50
var bird = 1000
var chirp = 0
var index = []
var cloud_speed = 0.1
var bt_speed = 0.5
var ft_speed = 2.5
var plane_speed = 5
var fence = new Image()
var tree_back = new Image()
var tree_front = new Image()
var grass = new Image()
var cloud = new Image()
var sky = new Image()
var field = new Image()
var road = new Image()
var plane = new Image()
var birds = new Image()
var train_audio = new Audio()
var plane_audio = new Audio()
var falcon = new Audio()
var chicks = new Audio()
var crows = new Audio()

for(var i=0; i<22; i++) {
	index[i] = new Image();
	index[i].src = 'images/bird'+i+'.png'
}

function init() {
	fence.src = 'images/fence.png'
	tree_back.src = 'images/tree_back.png'
	tree_front.src = 'images/tree_fwd.png'
	grass.src = 'images/grass.png'
	cloud.src = 'images/cloud.png'
	sky.src = 'images/sky.png'
	field.src = 'images/field.png'
	road.src = 'images/road.png'
	plane.src = 'images/plane.png'
	birds.src = 'images/birds.png'
	train_audio.src = 'sounds/train.mp3'
	plane_audio.src = 'sounds/plane.mp3'
	falcon.src = 'sounds/falcon.mp3'
	chicks.src = 'sounds/chicks.mp3'
	crows.src = 'sounds/crows.mp3'

	window.requestAnimationFrame(draw)
}

function draw() {
	var ctx = document.getElementById('canvas').getContext('2d')

	//Incrementing Drawing Position
	cloud1 += cloud_speed
	cloud2 += cloud_speed
	bt1 += bt_speed
	bt2 += bt_speed
	bt3 += bt_speed
	bt4 += bt_speed
	bt5 += bt_speed
	ft1 += ft_speed
	ft2 += ft_speed
	ft3 += ft_speed
	ft4 += ft_speed
	fencex += ft_speed
	chirp += 2
	bird -= 3

	//Drawing Images
	ctx.drawImage(sky, 0, 0)
	ctx.drawImage(road, 0, 428)
	ctx.drawImage(cloud, cloud1, 0)
	ctx.drawImage(cloud, cloud2, 0)
	ctx.drawImage(tree_back, bt1, 115)
	ctx.drawImage(tree_back, bt2, 115)
	ctx.drawImage(tree_back, bt3, 115)
	ctx.drawImage(tree_back, bt4, 115)
	ctx.drawImage(tree_back, bt5, 115)
	ctx.drawImage(grass, 0, 165)
	ctx.drawImage(field, 0, 265)
	ctx.drawImage(tree_front, ft1, 215)
	ctx.drawImage(tree_front, ft2, 215)
	ctx.drawImage(tree_front, ft3, 215)
	ctx.drawImage(tree_front, ft4, 215)
	ctx.drawImage(fence, fencex, 382)
	ctx.drawImage(fence, fencex-1000, 382);

	ctx.drawImage(birds, bird, 40, 100, 110)

	if(bt2 >= 400){
		airplane -= plane_speed
		if(airplane < 300) {
			air -= 1.4
			ctx.drawImage(plane, airplane, air, 90, 30)
		} else ctx.drawImage(plane, airplane, 50, 90, 30)			
		if(bt2 == 400 || bt2 == 700) plane_audio.play()		
	}

	if(chirp == 300) { 
		falcon.play()
	}
	if(chirp == 1000) {
		crows.play()
	}
	if(chirp == 3000) { 
		chicks.play()
	}

	//Looping through the canvas
	if(cloud2 > 1000) cloud2 = -70
	if(cloud1 > 1000) cloud1 = -70
	if(bt5 > 1000) bt5 = -90
	if(bt4 > 1000) bt4 = -90
	if(bt3 > 1000) bt3 = -90
	if(bt2 > 1000) bt2 = -90
	if(bt1 > 1000) bt1 = -90
	if(ft4 > 1000) ft4 = -200
	if(ft3 > 1000) ft3 = -200
	if(ft2 > 1000) ft2 = -200
	if(ft1 > 1000) ft1 = -200
	if(fencex > 1000) fencex = 0
	if (air < -35) air = 50
	if(airplane < 0) airplane = 3000
	if(chirp > 3000) chirp = -3000

	train_audio.play();
	train_audio.volume = 0.05
	window.requestAnimationFrame(draw)
}

init()